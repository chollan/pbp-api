#!/usr/bin/env bash

echo "migrate necessary data"
mysqldump -u root -p$MYSQL_PASSWORD pbpv3 add_method autopay_rule_transaction notification_method notification_subscription plaid_account plaid_item splitwise_token subscription_methods |mysql -u root -p$MYSQL_PASSWORD pbpv4

echo "set up table structure"
mysqldump --no-data -u root -p$MYSQL_PASSWORD pbpv3 account autopay_rule plaid_transaction transaction user  |mysql -u root -p$MYSQL_PASSWORD pbpv4

echo "change structure to int instead of double"
mysql -u root -p$MYSQL_PASSWORD pbpv4 -e 'ALTER TABLE `account` CHANGE `accrual` `accrual` INT NOT NULL;'
mysql -u root -p$MYSQL_PASSWORD pbpv4 -e 'ALTER TABLE `autopay_rule` CHANGE `amount` `amount` INT NOT NULL;'
mysql -u root -p$MYSQL_PASSWORD pbpv4 -e 'ALTER TABLE `plaid_transaction` CHANGE `amount` `amount` INT NOT NULL;'
mysql -u root -p$MYSQL_PASSWORD pbpv4 -e 'ALTER TABLE `transaction` CHANGE `beginningBalance` `beginningBalance` INT NOT NULL, CHANGE `amount` `amount` INT NOT NULL, CHANGE `endingBalance` `endingBalance` INT NOT NULL;'

#echo "delete unnecessary data"
#mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e 'DELETE FROM `user` WHERE last_login is NULL;'

echo "migrate account"
mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e "INSERT INTO pbpv4.account SELECT id, user_id, name, REGEXP_REPLACE(FORMAT(accrual, 2,'en_US'), '[\.,]', '') as accrual, locked, hidden, hash, bucket, added FROM pbpv3.account;"

echo "migrate autopay_rule"
mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e "INSERT INTO pbpv4.autopay_rule SELECT id, account_id, name, frequency,  REGEXP_REPLACE(FORMAT(amount, 2,'en_US'), '[\.,]', '') as amount, first_payment, active, rrule, next_run FROM pbpv3.autopay_rule;"

echo "migrate plaid_transaction"
mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e "INSERT INTO pbpv4.plaid_transaction SELECT id, user_id, account_id, transaction_id,  REGEXP_REPLACE(FORMAT(amount, 2,'en_US'), '[\.,]', '') as amount, date, ignore_transaction, description, pending FROM pbpv3.plaid_transaction;"

echo "migrate transaction"
mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e "INSERT INTO pbpv4.transaction SELECT id, account_id, add_method_id, description,  REGEXP_REPLACE(FORMAT(beginningBalance, 2,'en_US'), '[\.,]', '') as beginningBalance, REGEXP_REPLACE(FORMAT(amount, 2,'en_US'), '[\.,]', '') as amount, REGEXP_REPLACE(FORMAT(endingBalance, 2,'en_US'), '[\.,]', '') as endingBalance, date, reconciled, plaid_transaction_id FROM pbpv3.transaction;"

echo "migrate user"
mysql --init-command="SET FOREIGN_KEY_CHECKS=0;" -u root -p$MYSQL_PASSWORD pbpv4 -e "INSERT INTO pbpv4.user SELECT id, username, username_canonical, enabled,  salt,  REPLACE(password, '\$2y\$', '\$2a\$') as password, last_login, confirmation_token, password_requested_at, roles, firstName, lastName, active, lastSignIn, created, require_password_change, quick_login_token, plaid_item_limit, email, email_canonical FROM pbpv3.user;"