import { Injectable } from '@nestjs/common';
import {EntityManager} from 'typeorm';
import {User} from "../../entities/User";
@Injectable()
export class UserService{

  constructor(private em: EntityManager) {}

  async findUserById(id: number, includePassword: boolean = false){
    return await this.em.findOneBy(User, {id})
  }

  async findOneByEmail(email: string, includePass: boolean = false){
    const qb = this.em
      .getRepository(User)
      .createQueryBuilder('user');
    if(includePass){
      qb.addSelect("user.password")
    }
    qb.where({email})
    return await qb.getOneOrFail();
  }
}