import {Logger, Module} from '@nestjs/common';
import {UserService} from "./UserService";
import {UserController} from "./UserController";
import {AuthModule} from "../auth";


@Module({
  imports: [AuthModule],
  controllers: [UserController],
  providers: [UserService, Logger]
})
export class UserModule {}
