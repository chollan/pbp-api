import {BadRequestException, Controller, Get, Param, Req, UseGuards} from '@nestjs/common';
import { Request } from 'express';
import {UserService} from "./UserService";
import {AuthGuard} from "../auth/AuthGuard";

@Controller('users')
export class UserController {

  constructor(private userService: UserService) {}

  @Get('me')
  @UseGuards(AuthGuard)
  async me(
    @Req() request: Request
  ){
    return request['user'].toApiResponseModel();
  }
}