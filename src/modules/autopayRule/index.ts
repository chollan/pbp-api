import {Module} from '@nestjs/common';
import {UserService} from "../user/UserService";
import {UserModule} from "../user";
import TransactionsModule from "../transactions";
import {AccountServices} from "../accounts/AccountServices";
import {AutopayRuleController} from "./AutopayRuleController";
import {AutopayRuleService} from "./AutopayRuleService";


@Module({
  imports: [
    UserModule,
    TransactionsModule
  ],
  controllers: [
    AutopayRuleController
  ],
  providers: [
    AutopayRuleService,
    AccountServices,
    UserService
  ]
})
export class AutopayRuleModule{}