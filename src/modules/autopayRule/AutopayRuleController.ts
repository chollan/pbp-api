import {Controller, Get, Req, UseGuards} from "@nestjs/common";
import {AuthGuard} from "../auth/AuthGuard";
import {Request} from "express";
import {AutopayRuleService} from "./AutopayRuleService";

@Controller('autopay')
export class AutopayRuleController {

  constructor(private autopayRuleService: AutopayRuleService) {}

  @UseGuards(AuthGuard)
  @Get('')
  async getRules(
    @Req() request: Request
  ) {
    const rules = await this.autopayRuleService.getRulesForUser(request['user']);
    return rules.map(r => r.toApiResponseModel());
  }
}