import {Injectable} from "@nestjs/common";
import {EntityManager} from "typeorm";
import {User} from "../../entities/User";
import {AutopayRule} from "../../entities/AutopayRule";
import {Account} from "../../entities/Account";

@Injectable()
export class AutopayRuleService{

  constructor(private em: EntityManager) {}

  async getRulesForUser(user: User){
    const qb = this.em
      .getRepository(AutopayRule)
      .createQueryBuilder('ap')
      .innerJoin(Account, 'account')
      .where('account.user_id = :userId', {userId:user.id});

    return await qb.getMany();
  }
}