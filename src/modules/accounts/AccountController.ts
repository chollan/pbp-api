import {Controller, Get, Query, Req, UnauthorizedException, UseGuards} from "@nestjs/common";
import {Request} from "express";
import {AuthGuard} from "../auth/AuthGuard";
import {User} from "../../entities/User";
import {AccountServices} from "./AccountServices";
import {TransactionServices} from "../transactions/TransactionServices";

@Controller('accounts')
export class AccountController{

  constructor(
    private accountService: AccountServices,
    private transactionService: TransactionServices
  ) { }

  @Get('')
  @UseGuards(AuthGuard)
  async accounts(
    @Req() request: Request,
    @Query('transactions') transactions: string | undefined,
    @Query('id') acctId: number | undefined,
  ){
    const user:User = request['user'];
    let accounts;
    if(acctId){
      try{
        accounts = [await this.accountService.getUserAccountById(acctId, user)]
      }catch (e) {
        throw new UnauthorizedException('not authorized to this account');
      }
    }else{
      accounts = await this.accountService.getUserAccounts(user);
    }

    const result = await Promise.all(accounts.map(async account => {
      const acct = account.toApiResponseModel();

      let returnValue;
      if(transactions && transactions === '1' || transactions === 'true'){
        returnValue = {
          ...acct,
          balance: await this.transactionService.getAccountBalance(account),
          transactions: (await this.transactionService.getAccountTransactions(account, 1, 20)).map(t => t.toApiResponseModel())
        }
      }else{
        returnValue = {
          ...acct, balance: await this.transactionService.getAccountBalance(account)
        }
      }

      return returnValue;
    }));

    if(acctId){
      return result[0]
    }
    return result;
  }
}