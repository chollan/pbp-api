import {Module} from '@nestjs/common';
import {AccountServices} from "./AccountServices";
import {AccountController} from "./AccountController";
import {UserModule} from "../user";
import {UserService} from "../user/UserService";
import {TransactionServices} from "../transactions/TransactionServices";
import TransactionsModule from "../transactions";

@Module({
  imports: [
    UserModule,
    TransactionsModule
  ],
  controllers: [
    AccountController
  ],
  providers: [
    AccountServices,
    UserService,
    TransactionServices
  ]
})
export default class AccountsModule {}
