import {forwardRef, Inject, Injectable} from "@nestjs/common";
import {EntityManager} from "typeorm";
import {User} from "../../entities/User";
import {Account} from "../../entities/Account";
import {TransactionServices} from "../transactions/TransactionServices";
import {Transaction} from "../../entities/Transaction";

@Injectable()
export class AccountServices{

  constructor(
    private em: EntityManager
  ) {}

  async getUserAccounts(
    user:User
  ):Promise<Account[]> {
    const qb = this.em
      .getRepository(Account)
      .createQueryBuilder('a')
      .where({user})
      .orderBy('a.name');

    return await qb.getMany();
  }

  async getAccountById(id: number){
    return await this.em.findOne(Account, {
      where: {id}, relations: ['user']
    });
  }

  async getUserAccountById(id: number, user: User){
    const qb = this.em
      .getRepository(Account)
      .createQueryBuilder('a')
      .where({user})
      .andWhere({id})
      .orderBy('a.name')
      .take(1);

    return await qb.getOneOrFail();
  }
}