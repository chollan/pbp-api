import {Logger, Module} from '@nestjs/common';
import {JwtModule} from "@nestjs/jwt";
import {AuthController} from "./AuthController";
import {AuthService} from "./AuthService";
import {UserService} from "../user/UserService";
import {jwtConstants} from "./constants";


@Module({
  imports: [
    JwtModule.register({
      global: true,
      signOptions: { expiresIn: '185d', algorithm: 'RS256' },
      privateKey: jwtConstants.privateKey,
      publicKey: jwtConstants.publicKey,
    }),
  ],
  controllers: [
    AuthController
  ],
  providers: [
    AuthService,
    UserService,
    Logger
  ]
})
export class AuthModule {}
