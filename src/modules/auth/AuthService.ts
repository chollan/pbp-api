import {Injectable, Logger, UnauthorizedException} from '@nestjs/common';
import {UserService} from '../user/UserService';
import {JwtService} from "@nestjs/jwt";
import {compare} from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
    private logger: Logger
  ) {}

  async signIn(email: string, pass: string): Promise<{ access_token: string }> {
    let user;
    try{
      user = await this.usersService.findOneByEmail(email, true);
    }catch (e){
      this.logger.error(e)
      throw new UnauthorizedException();
    }

    if(user.active){
      if(!(await compare(pass, user.password))){

        this.logger.debug(`user ${email} password invalid`);
        throw new UnauthorizedException();
      }
    }else{
      this.logger.debug(`user ${email} is inactive`);
      throw new UnauthorizedException();
    }

    this.logger.debug(`user ${email} login success`);

    const access_token = this.jwtService.sign(user.toJwtAuthObject())
    return { access_token }
  }
}