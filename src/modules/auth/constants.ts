import * as process from "process";

export const jwtConstants = {
  publicKey: process.env.JWT_PUBLIC_KEY.replace(/\\n/g, '\n'),
  privateKey: process.env.JWT_PRIVATE_KEY.replace(/\\n/g, '\n')
};