import { Body, Controller, Post, HttpCode, HttpStatus } from '@nestjs/common';
import { AuthService } from './AuthService';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInRequest: {email: string, password: string}) {
    return this.authService.signIn(signInRequest.email, signInRequest.password);
  }
}