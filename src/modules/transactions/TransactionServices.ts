import {Injectable} from "@nestjs/common";
import {Account} from "../../entities/Account";
import {EntityManager} from "typeorm";
import {Transaction} from "../../entities/Transaction";

@Injectable()
export class TransactionServices {

  constructor(
    private em: EntityManager
  ) {}

  async getAccountTransactionCount(account: Account){
    const qb = this.em
      .getRepository(Transaction)
      .createQueryBuilder();
    const res = await qb
      .where({account: {id: account.id}})
      .orderBy('id', 'DESC')
      .getCount()
    return res;
  }

  async getAccountTransactions(account: Account, page: number = 1, perPage: number = 20) {
    const qb = this.em
      .getRepository(Transaction)
      .createQueryBuilder();
    const res = await qb
      .where({account: {id: account.id}})
      .skip((page - 1) * perPage)
      .take(perPage)
      .orderBy('id', 'DESC')
      .getMany();
    return res;
  }

  async getAccountBalance(account: Account) {
    const qb = this.em.getRepository(Transaction).createQueryBuilder();
    const res = await qb.select('endingBalance')
      .where({account})
      .limit(1)
      .orderBy('id', 'DESC')
      .getRawOne();

    return res?.endingBalance || 0;
  }
}