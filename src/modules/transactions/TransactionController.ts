import {Controller, Get, NotFoundException, Param, Query, Req, UnauthorizedException, UseGuards} from "@nestjs/common";;
import {AuthGuard} from "../auth/AuthGuard";
import {Request} from "express";
import {User} from "../../entities/User";
import {TransactionServices} from "./TransactionServices";
import {AccountServices} from "../accounts/AccountServices";

@Controller('transactions')
export class TransactionController{
  constructor(
    private transactionService: TransactionServices,
    private accountService: AccountServices
  ) { }

  @Get('')
  @UseGuards(AuthGuard)
  async accounts(
    @Req() request: Request,
    @Query('acct') acctId: number,
    @Query('page') page: number = 1,
    @Query('perPage') perPage: number = 20
  ){
    const user:User = request['user'];


    // get the users account
    const acct = await this.accountService.getAccountById(acctId);
    if(!acct){
      throw new NotFoundException('Account Not Found');
    }

    // validate the account belongs to the user
    if(acct.user.id !== user.id){
      throw new UnauthorizedException();
    }

    const [transactions, count] = await Promise.all([
      this.transactionService.getAccountTransactions( acct, page, perPage ),
      this.transactionService.getAccountTransactionCount( acct )
    ]);

    // return to a response model
    return {
      count, perPage,
      page: parseInt(String(page)),
      pages: Math.ceil(count/perPage),
      tx: transactions.map(t => t.toApiResponseModel())
    }
  }
}