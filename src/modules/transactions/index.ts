import {Module} from '@nestjs/common';
import {UserModule} from "../user";
import {UserService} from "../user/UserService";
import {TransactionController} from "./TransactionController";
import {TransactionServices} from "./TransactionServices";
import {AccountServices} from "../accounts/AccountServices";

@Module({
  imports: [
    UserModule,
  ],
  controllers: [
    TransactionController
  ],
  providers: [
    TransactionServices,
    UserService,
    AccountServices
  ]
})
export default class TransactionsModule {}