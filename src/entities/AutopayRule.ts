import {Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Account} from "./Account";
import {Transaction} from "./Transaction";

@Entity()
export class AutopayRule{

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Account)
  @JoinColumn({ name: "account_id" })
  account: Account;

  @Column()
  name: string;

  @Column()
  frequency: number;

  @Column()
  amount: number;

  @Column({type: "date", name: 'first_payment'})
  firstPayment: Date;

  @Column({default: true})
  active: boolean;

  @Column({length: 255})
  rrule: string;

  @Column({type: "date", name: 'next_run'})
  nextRun: Date;

  @ManyToMany(() => Transaction)
  @JoinTable()
  transactions: Transaction[];

  toApiResponseModel() {
    return {
      id: this.id,
      name: this.name,
      amount: this.amount,
      firstPayment: this.firstPayment,
      active: this.active,
      account: this.account,
      rrule: this.rrule,
      nextRun: this.nextRun,
      transactions: this.transactions,
    }
  }
}