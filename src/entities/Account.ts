import {
  Column,
  CreateDateColumn,
  Entity, JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import {User} from "./User";
import {Transaction} from "./Transaction";


@Entity()
export class Account {

  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne(() => User, (user) => user.accounts)
  @JoinColumn({ name: "user_id" })
  user: User

  @Column({
    length: 100,
  })
  name: string

  @Column()
  accrual: number

  @Column()
  locked: boolean

  @Column()
  hidden: boolean

  @Column({
    length: 10,
    nullable: true
  })
  hash: string

  @Column()
  bucket: boolean

  @CreateDateColumn({type: 'datetime'})
  added: Date

  @OneToMany(() => Transaction, (transaction) => transaction.account)
  transactions: Transaction[]

  toApiResponseModel(){
    return {
      id: this.id,
      name: this.name,
      accrual: this.accrual,
      hash: this.hash,
      bucket: this.bucket
    }
  }
}