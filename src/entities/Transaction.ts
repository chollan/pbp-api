import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Account} from "./Account";

export const PAYDAY_DESCRIPTION = 'Payday';
export const TYPE_DEBT = 'debt';
export const TYPE_CREDIT = 'credit';
export const ADD_METHOD_AUTOPAY = 3;
export const ADD_METHOD_TRANSFER = 4;
export const ADD_METHOD_ACCRUAL = 6;

@Entity({orderBy:{id:"DESC"}})
export class Transaction{

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Account, (account) => account.transactions)
  @JoinColumn({ name: "account_id" })
  account: Account;

  @Column({name: 'add_method_id', nullable: true})
  addMethod: number;

  @Column("longtext")
  description: string;

  @Column()
  beginningBalance: number;

  @Column()
  amount: number;

  @Column()
  endingBalance: number;

  @Column({type: 'datetime'})
  date: Date;

  @Column()
  reconciled: boolean;

  @Column({name: 'plaid_transaction_id', nullable: true})
  plaidTransactionId: string

  toApiResponseModel(){
    return {
      id: this.id,
      description: this.description,
      amount: this.amount,
      endingBalance: this.endingBalance,
      addMethod: this.addMethod,
      date: this.date,
      reconciled: this.reconciled
    }
  }

}