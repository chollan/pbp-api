import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, CreateDateColumn} from "typeorm"
import {SplitwiseToken} from "./SplitwiseToken";
import {Account} from "./Account";

@Entity()
export class User{

    @PrimaryGeneratedColumn()
    id: number

    @Column({ unique: true })
    email: string

    @Column({ select: false })
    password: string

    @Column()
    firstName: string

    @Column()
    lastName: string

    @Column({default: true})
    active: boolean

    @Column({nullable: true})
    lastSignIn: Date

    @CreateDateColumn()
    created: Date

    @Column({default: false, name: 'require_password_change'})
    requirePasswordChange: boolean

    @OneToMany(() => Account, (account) => account.user)
    accounts: Account[]

    @OneToOne(() => SplitwiseToken)
    splitwiseToken: SplitwiseToken

    @Column({nullable: true, name: 'quick_login_token'})
    quickLoginToken: string

    @Column({nullable: true, name: 'plaid_item_limit'})
    plaidItemLimit: number

    toApiResponseModel() {
        return {
            id: this.id,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            active: this.active,
            lastSignIn: this.lastSignIn,
            created: this.created,
        }
    }

    toJwtAuthObject() {
        return { id: this.id };
    }
}
