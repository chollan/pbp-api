import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";

@Entity()
export class SplitwiseToken{
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.splitwiseToken)
  @JoinColumn({ name: "user__id" })
  user: User;

  @Column()
  token: string;

  @Column({name: 'splitwise_user_id'})
  splitwiseUserId: number;

}