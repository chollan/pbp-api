import {Money as TSMoney} from "ts-money";
import {
  multiply
} from 'mathjs'

export class Money{
  static toMoney(int: number){
    return new TSMoney(int, 'USD');
  }
}