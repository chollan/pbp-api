import 'dotenv/config'
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Account} from "./entities/Account";
import {AutopayRule} from "./entities/AutopayRule";
import {SplitwiseToken} from "./entities/SplitwiseToken";
import {Transaction} from "./entities/Transaction";
import {User} from "./entities/User";
import {AuthModule} from "./modules/auth"
import {UserModule} from "./modules/user";
import AccountsModule from "./modules/accounts";
import TransactionsModule from "./modules/transactions";
import {AutopayRuleModule} from "./modules/autopayRule";
import * as process from "process";

@Module({
  imports: [
    UserModule, AccountsModule,
    TransactionsModule, AuthModule,
    AutopayRuleModule,
    TypeOrmModule.forRoot({
      type: "mysql",
      url: `mysql://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/${process.env.DB_NAME}`,
      logging: true,
      entities: [
        Account,
        AutopayRule,
        SplitwiseToken,
        Transaction,
        User
      ],
      migrations: [],
      subscribers: []
    })
  ],
  controllers: [],
  providers: []
})
export class AppModule {}
